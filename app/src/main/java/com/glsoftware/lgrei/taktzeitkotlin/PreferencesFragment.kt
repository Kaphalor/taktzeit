package com.glsoftware.lgrei.taktzeitkotlin

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompat
import android.text.InputType
import com.example.lgrei.taktzeitkotlin.R

class PreferencesFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener{

    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }

    private lateinit var category1 : android.support.v7.preference.Preference
    private lateinit var category2 : android.support.v7.preference.Preference
    private lateinit var category3 : android.support.v7.preference.Preference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(this)

        category1 = findPreference(getString(R.string.preferencePause1Category))
        category2 = findPreference(getString(R.string.preferencePause2Category))
        category3 = findPreference(getString(R.string.preferencePause3Category))

        useNumberBlock(PreferenceManager.getDefaultSharedPreferences(context))
    }

    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String){

        if (key == getString(R.string.preferencePausenSumme)) pausenSumme(key, 0)
        else if (key == getString(R.string.useNumberBlockKey)){
            useNumberBlock(sharedPreferences)
        }
    }

    private fun pausenSumme(key: String, override: Int){
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val pausenSumme: Int? = if (override in 1..3) override
            else{
            sharedPref?.getString(key, getString(R.string.preferencePausenSummeDefault))?.toInt()
        }

        when (pausenSumme) {
            0 -> {
                preferenceScreen.removePreference(category1)
                preferenceScreen.removePreference(category2)
                preferenceScreen.removePreference(category3)
            }
            1 -> {
                preferenceScreen.addPreference(category1)
                preferenceScreen.removePreference(category2)
                preferenceScreen.removePreference(category3)
            }
            2 -> {
                preferenceScreen.addPreference(category1)
                preferenceScreen.addPreference(category2)
                preferenceScreen.removePreference(category3)
            }
            3 -> {
                preferenceScreen.addPreference(category1)
                preferenceScreen.addPreference(category2)
                preferenceScreen.addPreference(category3)
            }
        }
    }

    private fun useNumberBlock(sharedPreferences: SharedPreferences){
        val check = sharedPreferences.
                getBoolean(getString(R.string.useNumberBlockKey), true)

        if (check){
            pausenSumme(getString(R.string.preferencePausenSumme), 3)

            var edit = findPreference(getString(R.string.preferenceTaktDauer))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType =
                    InputType.TYPE_DATETIME_VARIATION_TIME or InputType.TYPE_CLASS_DATETIME

            edit = findPreference(getString(R.string.preferenceDayStart))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType =
                    InputType.TYPE_DATETIME_VARIATION_TIME or InputType.TYPE_CLASS_DATETIME

            edit = findPreference(getString(R.string.preferenceDayEnd))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType =
                    InputType.TYPE_DATETIME_VARIATION_TIME or InputType.TYPE_CLASS_DATETIME

            edit = findPreference(getString(R.string.preferencePause1StartEdit))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType =
                    InputType.TYPE_DATETIME_VARIATION_TIME or InputType.TYPE_CLASS_DATETIME

            edit = findPreference(getString(R.string.preferencePause1EndEdit))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType =
                    InputType.TYPE_DATETIME_VARIATION_TIME or InputType.TYPE_CLASS_DATETIME

            edit = findPreference(getString(R.string.preferencePause2StartEdit))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType =
                    InputType.TYPE_DATETIME_VARIATION_TIME or InputType.TYPE_CLASS_DATETIME

            edit = findPreference(getString(R.string.preferencePause2EndEdit))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType =
                    InputType.TYPE_DATETIME_VARIATION_TIME or InputType.TYPE_CLASS_DATETIME

            edit = findPreference(getString(R.string.preferencePause3StartEdit))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType =
                    InputType.TYPE_DATETIME_VARIATION_TIME or InputType.TYPE_CLASS_DATETIME

            edit = findPreference(getString(R.string.preferencePause3EndEdit))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType =
                    InputType.TYPE_DATETIME_VARIATION_TIME or InputType.TYPE_CLASS_DATETIME

            pausenSumme(getString(R.string.preferencePausenSumme), 0)
        } else {
            pausenSumme(getString(R.string.preferencePausenSumme), 3)

            var edit = findPreference(getString(R.string.preferenceTaktDauer))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType = InputType.TYPE_CLASS_TEXT

            edit = findPreference(getString(R.string.preferenceDayStart))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType = InputType.TYPE_CLASS_TEXT

            edit = findPreference(getString(R.string.preferenceDayEnd))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType = InputType.TYPE_CLASS_TEXT

            edit = findPreference(getString(R.string.preferencePause1StartEdit))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType = InputType.TYPE_CLASS_TEXT

            edit = findPreference(getString(R.string.preferencePause1EndEdit))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType =
                    InputType.TYPE_CLASS_TEXT

            edit = findPreference(getString(R.string.preferencePause2StartEdit))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType =
                    InputType.TYPE_CLASS_TEXT

            edit = findPreference(getString(R.string.preferencePause2EndEdit))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType =
                    InputType.TYPE_CLASS_TEXT

            preferenceScreen.addPreference(category3)
            edit = findPreference(getString(R.string.preferencePause3StartEdit))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType =
                    InputType.TYPE_CLASS_TEXT

            edit = findPreference(getString(R.string.preferencePause3EndEdit))
                    as com.takisoft.fix.support.v7.preference.EditTextPreference
            edit.editText.inputType =
                    InputType.TYPE_CLASS_TEXT
            preferenceScreen.removePreference(category3)

            pausenSumme(getString(R.string.preferencePausenSumme), 0)
        }
    }
}
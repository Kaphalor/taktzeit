package com.glsoftware.lgrei.taktzeitkotlin

import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import com.example.lgrei.taktzeitkotlin.R
import java.util.*


class MainActivity : AppCompatActivity() {

    private var withEditText = false
    private lateinit var mAdView: AdView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false)

        setSupportActionBar(my_toolbar)
        checkNumberBlock()

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        if (!(sharedPref.getBoolean(getString(R.string.firstOpened), true))) taktzeit()

        editTime.setOnEditorActionListener { _, actionID, _ ->
            if(actionID == EditorInfo.IME_ACTION_DONE){
                withEditText = true
                taktzeit()
                with (sharedPref.edit()){
                    putBoolean(getString(R.string.firstOpened), false).apply()
                }
                withEditText = false
                true
            } else{
                false
            }
        }
        MobileAds.initialize(this, "ca-app-pub-3337231624077840~7822738569")
        mAdView = findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)
    }

    private var inPreference = false
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        when (id){
            R.id.action_settings -> {
                if (!inPreference) {
                    fragment_container.setBackgroundResource(android.R.color.white)
                    supportFragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, PreferencesFragment())
                            .addToBackStack(null).commit()
                    supportActionBar?.setDisplayHomeAsUpEnabled(true)
                    inPreference = true
                }
                return true
            }
            android.R.id.home -> {
                supportFragmentManager.popBackStack()
                fragment_container.setBackgroundResource(android.R.color.transparent)
                supportActionBar?.setDisplayHomeAsUpEnabled(false)
                inPreference = false
                checkNumberBlock()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0){
            supportFragmentManager.popBackStack()
            fragment_container.setBackgroundResource(android.R.color.transparent)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            inPreference = false
            checkNumberBlock()
        } else {
            return super.onBackPressed()
        }
    }

    private fun timeStringToInt(timeString: String?) : Int {
         return if (timeString != null) {
            when (timeString.contains(":")) {
                true -> {
                    val splitString = timeString.split(":")
                    (splitString[0].toInt() * 60) + splitString[1].toInt()
                }
                false -> timeString.toInt()
            }
        } else 0
    }

    private fun checkNumberBlock(){
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val check = sharedPref.getBoolean(getString(R.string.useNumberBlockKey), true)

        if(check){
            editTime.inputType =
                    InputType.TYPE_DATETIME_VARIATION_TIME or InputType.TYPE_CLASS_DATETIME
        }
        else editTime.inputType = InputType.TYPE_CLASS_TEXT
    }

    private fun taktzeit() {
        showTaktzeit.setText(R.string.ihreZeiten)
        val format = SimpleDateFormat("HH:mm")
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val currentTime = Calendar.getInstance()

        val dayKey = getString(R.string.day)
        val day = Calendar.getInstance()
        var dayString = sharedPref.getString(dayKey, "365:4000")
        val daySplit = dayString?.split(":")
        if (daySplit != null) {
            day.set(Calendar.DAY_OF_YEAR, daySplit[0].toInt())
            day.set(Calendar.YEAR, daySplit[1].toInt())

        }
        if (withEditText || day.get(Calendar.DAY_OF_YEAR) < currentTime.get(Calendar.DAY_OF_YEAR) ||
                        day.get(Calendar.YEAR) < currentTime.get(Calendar.YEAR)) {

            var numInput : Int

            val now = Calendar.getInstance()
            val test = Calendar.getInstance()

            val pausenSummeKey = getString(R.string.preferencePausenSumme)
            val pausenSummeDefault = getString(R.string.preferencePausenSummeDefault)
            val pausenSumme = sharedPref?.getString(pausenSummeKey, pausenSummeDefault)?.toInt()

            val pause1Start = Calendar.getInstance()
            val pause1End = Calendar.getInstance()
            val pause2Start = Calendar.getInstance()
            val pause2End = Calendar.getInstance()
            val pause3Start = Calendar.getInstance()
            val pause3End = Calendar.getInstance()

            val timeMap : MutableSet<String> = mutableSetOf()

            if (pausenSumme != null && pausenSumme >= 1) {
                    var startKey = getString(R.string.preferencePause1StartEdit)
                    var startDefault = getString(R.string.preferencePause1StartEditDefault)
                    var startString = sharedPref.getString(startKey, startDefault)
                    var endKey = getString(R.string.preferencePause1EndEdit)
                    var endDefault = getString(R.string.preferencePause1EndEditDefault)
                    var endString = sharedPref.getString(endKey, endDefault)

                if (startString != null && endString != null) {
                    pause1Start.set(Calendar.HOUR_OF_DAY, startString.split(":")[0].toInt())
                    pause1Start.set(Calendar.MINUTE, startString.split(":")[1].toInt())
                    pause1End.set(Calendar.HOUR_OF_DAY, endString.split(":")[0].toInt())
                    pause1End.set(Calendar.MINUTE, endString.split(":")[1].toInt())
                }

                    if (pausenSumme >= 2) {
                        startKey = getString(R.string.preferencePause2StartEdit)
                        startDefault = getString(R.string.preferencePause2StartEditDefault)
                        startString = sharedPref.getString(startKey, startDefault)
                        endKey = getString(R.string.preferencePause2EndEdit)
                        endDefault = getString(R.string.preferencePause2EndEditDefault)
                        endString = sharedPref.getString(endKey, endDefault)

                        if (startString != null && endString != null) {
                            pause2Start.set(Calendar.HOUR_OF_DAY, startString.split(":")[0].toInt())
                            pause2Start.set(Calendar.MINUTE, startString.split(":")[1].toInt())
                            pause2End.set(Calendar.HOUR_OF_DAY, endString.split(":")[0].toInt())
                            pause2End.set(Calendar.MINUTE, endString.split(":")[1].toInt())
                        }

                        if (pausenSumme >= 3) {
                            startKey = getString(R.string.preferencePause3StartEdit)
                            startDefault = getString(R.string.preferencePause3StartEditDefault)
                            startString = sharedPref.getString(startKey, startDefault)
                            endKey = getString(R.string.preferencePause3EndEdit)
                            endDefault = getString(R.string.preferencePause3EndEditDefault)
                            endString = sharedPref.getString(endKey, endDefault)

                            if (startString != null && endString != null) {
                                pause3Start.set(Calendar.HOUR_OF_DAY,
                                        startString.split(":")[0].toInt())
                                pause3Start.set(Calendar.MINUTE,
                                        startString.split(":")[1].toInt())
                                pause3End.set(Calendar.HOUR_OF_DAY,
                                        endString.split(":")[0].toInt())
                                pause3End.set(Calendar.MINUTE,
                                        endString.split(":")[1].toInt())
                            }
                        }
                    }
                }
            val dayStart = Calendar.getInstance()
            val dayEnd = Calendar.getInstance()
            var dayStartEndKey = getString(R.string.preferenceDayStart)
            var dayStartEndDefault = getString(R.string.preferenceDayStartDefault)
            var dayStartEndString = sharedPref.getString(dayStartEndKey, dayStartEndDefault)

            if (dayStartEndString != null) {
                dayStart.set(Calendar.HOUR_OF_DAY,
                            dayStartEndString.split(":")[0].toInt())
                dayStart.set(Calendar.MINUTE, dayStartEndString.split(":")[1].toInt())
            }

            dayStartEndKey = getString(R.string.preferenceDayEnd)
            dayStartEndDefault = getString(R.string.preferenceDayEndDefault)
            dayStartEndString = sharedPref.getString(dayStartEndKey, dayStartEndDefault)

            if (dayStartEndString != null) {
                dayEnd.set(Calendar.HOUR_OF_DAY, dayStartEndString.split(":")[0].toInt())
                dayEnd.set(Calendar.MINUTE, dayStartEndString.split(":")[1].toInt())
            }

            var timeLeft : Int

            numInput = if (day.get(Calendar.DAY_OF_YEAR) < currentTime.get(Calendar.DAY_OF_YEAR) ||
                    day.get(Calendar.YEAR) < currentTime.get(Calendar.YEAR)){

                sharedPref.getInt(getString(R.string.timeLeft), 0)
            } else timeStringToInt(editTime.text.toString())

            if (withEditText) {
                now.set(Calendar.HOUR_OF_DAY, dayStart.get(Calendar.HOUR_OF_DAY))
                now.set(Calendar.MINUTE, dayStart.get(Calendar.MINUTE))
            } else {
                now.set(Calendar.HOUR_OF_DAY, currentTime.get(Calendar.HOUR_OF_DAY))
                now.set(Calendar.MINUTE, currentTime.get(Calendar.MINUTE))
            }

            test.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY))
            test.set(Calendar.MINUTE, now.get(Calendar.MINUTE))
            val lastTime = Calendar.getInstance()

            var counter = 1
            var pauseHour: Int
            var pauseMinute: Int

            while (now < dayEnd) {
                if (counter == 2) {
                    val taktDauerKey = getString(R.string.preferenceTaktDauer)
                    val taktDauerDefault = getString(R.string.preferenceTaktDauerDefault)
                    numInput = timeStringToInt(sharedPref.getString(taktDauerKey, taktDauerDefault))
                }

                test.add(Calendar.MINUTE, numInput)

                if (pausenSumme != null) {
                    if (now < pause1Start && pause1Start <= test && pausenSumme >= 1) {
                        pauseHour = pause1End.get(Calendar.HOUR_OF_DAY) - pause1Start.get(Calendar.HOUR_OF_DAY)
                        pauseMinute = pause1End.get(Calendar.MINUTE) - pause1Start.get(Calendar.MINUTE)

                        now.add(Calendar.MINUTE, (pauseHour * 60 + pauseMinute))
                        test.add(Calendar.MINUTE, (pauseHour * 60 + pauseMinute))
                    }
                }
                if (pausenSumme != null) {
                    if (now < pause2Start && pause2Start <= test && pausenSumme >= 2) {
                        pauseHour = pause2End.get(Calendar.HOUR_OF_DAY) - pause2Start.get(Calendar.HOUR_OF_DAY)
                        pauseMinute = pause2End.get(Calendar.MINUTE) - pause2Start.get(Calendar.MINUTE)

                        now.add(Calendar.MINUTE, (pauseHour * 60 + pauseMinute))
                        test.add(Calendar.MINUTE, (pauseHour * 60 + pauseMinute))
                    }
                }
                if (pausenSumme != null) {
                    if (now < pause3Start && pause3Start <= test && pausenSumme >= 3) {
                        pauseHour = pause2End.get(Calendar.HOUR_OF_DAY) - pause2Start.get(Calendar.HOUR_OF_DAY)
                        pauseMinute = pause2End.get(Calendar.MINUTE) - pause2Start.get(Calendar.MINUTE)

                        now.add(Calendar.MINUTE, (pauseHour * 60 + pauseMinute))
                        test.add(Calendar.MINUTE, (pauseHour * 60 + pauseMinute))
                    }
                }

                now.add(Calendar.MINUTE, numInput)

                if (now in currentTime..dayEnd) {
                    if (!withEditText){
                        timeLeft = sharedPref.getInt(getString(R.string.timeLeft), 0)
                        showTaktzeit.append(getString(R.string.estimatedTime, timeLeft))
                    }
                    showTaktzeit.append(getString(R.string.printTime, counter, format.format(now.time)))
                    timeMap.add(format.format(now.time))

                    counter++
                    lastTime.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY))
                    lastTime.set(Calendar.MINUTE, now.get(Calendar.MINUTE))
                }
            }

            timeLeft = (dayEnd.get(Calendar.HOUR_OF_DAY) - lastTime.get(Calendar.HOUR_OF_DAY)) * 60
            timeLeft += dayEnd.get(Calendar.MINUTE) - lastTime.get(Calendar.MINUTE)

            dayString = currentTime.get(Calendar.DAY_OF_YEAR).toString() + ":" +
                    currentTime.get(Calendar.YEAR).toString()

            with(sharedPref.edit()) {
                putInt(getString(R.string.timeLeft), timeLeft).apply()
                putString(getString(R.string.day), dayString).apply()
                putBoolean(getString(R.string.firstOpened), true).apply()
                putStringSet(getString(R.string.timeMapKey), timeMap).apply()
            }
        } else {
            val ups = mutableSetOf<String>(getString(R.string.ups))
            val set = sharedPref.getStringSet(getString(R.string.timeMapKey), ups)
            val list = set?.toList() ?: listOf("0")
            val listSize = list.size

            val time = Calendar.getInstance()

            var counter = 1
            var listCounter = listSize - 1
            var printCounter = 1
            while (counter <= listSize) {
                time.set(Calendar.HOUR_OF_DAY, list[listCounter].split(":")[0].toInt())
                time.set(Calendar.MINUTE, list[listCounter].split(":")[1].toInt())

                if (time >= currentTime) {
                    showTaktzeit.append(getString
                        (R.string.printTime, printCounter, list[listCounter]))
                    printCounter++
                }

                counter++
                listCounter--
            }
        }
    }
}
